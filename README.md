#### Program
- Install Node
- Install Git
- git clone https://gitlab.com/ariya925/my_project.git

#### My SQL
- Create Database name "my_project"
- Import Database

#### Post Man
- Import My Project.postman_collection.json

#### Code
- NPM i
- Install nodemon
- Create .env
host=localhost
user=root
password=
database=my_project
- Run "npm run dev"
- if show "Connected!" is success


#### Authentication
สามารถสมัครสมาชิกได้
- Register (Email, Password)
- มีระบบ Login
- Login (Check PassWord)

#### User management
- เรียกดู Profile ของตัวเอง
- เรียกดู Order history ของตัวเอง
- เรียกดู ผู้ใช้งานทั้งระบบ (Add on)
- เรียกดู ผู้ใช้งาน สถานะ active (Add on)
- เรียกดู ผู้ใช้งาน สถานะ inactive (Add on)
- อับเดท email ผู้ใช้งาน email (Add on)
- เปลี่ยนรหัสผ่านผู้ใช้งาน (ยังไม่ติดตั้ง email ของ admin ทำให้ไม่สามารถส่งข้อมูลไปที่ email user ได้ รวมไปถึงเพิ่ม member_id เพื่อตรวจสอบ user อีกครั้งหนึ่ง) (Add on)

#### Product management
- เรียกดู Product ทั้งหมดที่มีในระบบ
- สามารถดูข้อมูลของแต่ละ Product ได้
- อับเดท ชื่อของผลิตภัณฑ์ (Add on)
- อับเดท สถานะของผลิตภัณฑ์ (Add on)
- เรียกดู สถานะที่ active ผลิตภัณฑ์ (Add on)
- เรียกดู สถานะที่ inactive ผลิตภัณฑ์ (Add on)
- สร้างผลิตภัณฑ์ใหม่ (Add on)

#### Order management
- สร้างรายการคำสั่งซื้อได้
- ยกเลิกคำสั่งซื้อ
- ดูรายละเอียดคำสั่งซื้อ
- ดูคำสั่งซื้อทั้งหมด (Add on)
- ดูคำสั่งซื้อ ทีละคำสั่งซื้อได้ (Add on)

#### Admin management (Add on)
- อับเดท สถานะของผู้ใช้งาน (ตั้งค่าระบบให้ user ที่มี status inactive เข้าระบบไม่ได้ ) (Add on)
