var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

require("./config/db")

var userRouter = require("./routes/user");
var productRouter = require("./routes/products");
var orderRouter = require("./routes/order");
var statusRouter = require("./routes/status");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/user", userRouter);
app.use("/products", productRouter);
app.use("/order", orderRouter);
app.use("/status", statusRouter);

module.exports = app;
