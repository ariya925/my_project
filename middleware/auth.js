const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send({ "data": "A token is required for authentication" });
    }

    try {
        jwt.verify(token, 'shhhhh');
        next();
    } catch (error) {
        return res.status(401).send({ "data": "Invalid Token" });
    }
};

module.exports = verifyToken;