var con = require("../config/db");

var queryData = (sql) => {
  return new Promise(function (resolve, reject) {
    con.query(sql, (err, result) => {
      if (err) return reject(err)
      resolve(result)
    })
  })
};

var getDateFormatTH = (date) => {
  if (date === null) {
    return '';
  } else {
    let timeZoneThailand = new Date(date).toLocaleString('th-TH', { timeZone: 'Asia/Bangkok' });
    return timeZoneThailand;
  }

}

module.exports = {
  queryData,
  getDateFormatTH
};
