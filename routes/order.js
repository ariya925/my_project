var express = require("express");
var router = express.Router();

const auth = require("../middleware/auth");

const { queryData, getDateFormatTH } = require("../model/db")

router.get("/all", auth, async (req, res) => {
  try {

    let queryOrders = `SELECT users.email, orders.id AS order_id, status.name AS status_name ,orders.date_create,orders.date_modify FROM orders LEFT JOIN users ON users.id = orders.user_id LEFT JOIN status ON status.id = orders.status;`;

    let orders = await queryData(queryOrders);

    if (typeof orders !== 'undefined' && orders.length > 0) {
      let arrOfData = [];

      for (let i = 0; i < orders.length; i++) {
        let data = {
          "order_id": orders[i].order_id,
          "email": orders[i].email,
          "status": orders[i].status_name,
          "date_create": getDateFormatTH(orders[i].date_create),
          "date_modify": getDateFormatTH(orders[i].date_modify)
        }
        arrOfData.push(data);
      }

      res.json(
        {
          data: arrOfData
        }
      )
    } else {
      res.json(
        {
          data: "We dont have order"
        }
      )
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/:id", auth, async (req, res) => {
  try {

    let { id } = req.params;

    // Validate input
    if (!(id)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (isNaN(parseInt(id))) {
      res.status(400).send({ "data": "All input is required" });
    } else {
      let queryProductsOfOrder = `SELECT orders.id AS order_id ,products.id AS product_id,products.name AS product_name,status.name AS status_name ,orders.date_create FROM orders_products LEFT JOIN orders ON orders.id = orders_products.order_id LEFT JOIN products ON products.id = orders_products.product_id LEFT JOIN status ON status.id = orders.status WHERE orders.id =${id};`;
      let products = await queryData(queryProductsOfOrder);

      if (typeof products !== 'undefined' && products.length > 0) {

        let orderID = products[0].order_id

        let arrOfData = {
          "order_id": orderID,
          "data": []
        }

        for (let i = 0; i < products.length; i++) {
          let data = {
            "product_id": products[i].product_id,
            "product_name": products[i].product_name,
            "status": products[i].status_name,
            "date_create": getDateFormatTH(products[i].date_create),
          }
          arrOfData.data.push(data);

        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: 'We dont have order'
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/status/:status_id", auth, async (req, res) => {
  try {

    let { status_id } = req.params;

    // Validate input
    if (!(status_id)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (isNaN(parseInt(status_id))) {
      res.status(400).send({ "data": "All input is required" });
    } else {
      let queryProductsOfOrder = `SELECT orders.id AS order_id ,products.id AS product_id,products.name AS product_name,status.name AS status_name,orders.date_create FROM orders_products LEFT JOIN orders ON orders.id = orders_products.order_id LEFT JOIN products ON products.id = orders_products.product_id LEFT JOIN status ON status.id = orders.status WHERE orders.status =${status_id} ORDER BY orders_products.date_create DESC;`;
      let products = await queryData(queryProductsOfOrder);

      if (typeof products !== 'undefined' && products.length > 0) {

        let queryOrderGroupBy = `SELECT orders.id AS order_id FROM orders_products LEFT JOIN orders ON orders.id = orders_products.order_id LEFT JOIN products ON products.id = orders_products.product_id LEFT JOIN status ON status.id = orders.status WHERE orders.status =${status_id} GROUP BY order_id ORDER BY orders_products.date_create DESC;`;
        let orderGroupBy = await queryData(queryOrderGroupBy);

        let arrOfData = [];

        for (let j = 0; j < orderGroupBy.length; j++) {
          let arrOfOrder_id = {
            "order_id": orderGroupBy[j].order_id,
          }
          arrOfData.push(arrOfOrder_id);
        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: 'We dont have order'
          }
        )
      }
    }



  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.post("/", auth, async (req, res) => {
  try {
    let { userID, order } = req.body;

    if (!(userID) && userID === "") {
      res.status(400).json({ data: "All input is required" });
    } else if (isNaN(parseInt(userID))) {
      res.status(400).json({ data: "All input is required" });
    } else if (typeof order !== "object") {
      res.status(400).json({ data: "All input is required" });
    } else {
      let queryUser = `SELECT id FROM users WHERE id =${userID};`;
      let user = await queryData(queryUser)

      if (typeof user !== 'undefined' && user.length == 0) {
        res.json({ data: "We dont have user" });
      } else if (typeof order !== 'undefined' && order.length == 0) {
        res.json({ data: "order empty" });
      } else {

        let dontHaveProduct = ''
        for (let i = 0; i < order.length; i++) {

          var queryOrder = `SELECT id FROM products WHERE id ='${order[i]}';`;
          var orderOfID = await queryData(queryOrder)
          if (typeof orderOfID !== 'undefined' && orderOfID.length == 0) {
            dontHaveProduct = true;
            break;
          }
        }

        if (dontHaveProduct) {
          res.send({ "data": "We dont have product" })
        } else {
          let queryLastOrderIDActiveOfUserID = `SELECT id FROM orders WHERE user_id =${userID} AND status = 1 ORDER BY id DESC LIMIT 1;`;
          let checkLastOrderIDActive = await queryData(queryLastOrderIDActiveOfUserID)

          // user have order active?
          if (typeof checkLastOrderIDActive !== 'undefined' && checkLastOrderIDActive.length == 0) {
            let queryInsertNewOrder = `INSERT INTO orders(user_id) VALUES (${userID});`;
            await queryData(queryInsertNewOrder);
          }

          let lastOrderIDActive = await queryData(queryLastOrderIDActiveOfUserID)

          for (let i = 0; i < order.length; i++) {
            let queryInsertOrdersProducts = `INSERT INTO orders_products(order_id,product_id) VALUES (${lastOrderIDActive[0].id},${order[i]});`;
            await queryData(queryInsertOrdersProducts);
          }

          res.status(201).json(
            {
              data: 'create order success'
            }
          )
        }


      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.put("/cancle", auth, async (req, res) => {
  try {
    let { userID, orderID } = req.body;

    // Validate input
    if (!(userID) && !(orderID)) {
      res.status(400).json({ data: "All input is required" });
    } else if (isNaN(parseInt(userID)) && isNaN(parseInt(orderID))) {
      res.status(400).json({ data: "All input is required" });
    } else {
      let queryOrderOfUser = `SELECT id FROM orders WHERE user_id ='${userID}' AND id ='${orderID}';`;
      let orderOfUser = await queryData(queryOrderOfUser)

      if (typeof orderOfUser !== 'undefined' && orderOfUser.length > 0) {
        let queryUpdateStatusOfOrder = `UPDATE orders SET status = 2 WHERE id = ${orderID} AND user_id =${userID};`
        await queryData(queryUpdateStatusOfOrder);
        res.json(
          {
            data: 'cancle order success'
          }
        )
      } else {
        res.json(
          {
            data: 'We dont have order'
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

module.exports = router;
