var express = require("express");
var router = express.Router();

const auth = require("../middleware/auth");

const { queryData, getDateFormatTH } = require("../model/db")

router.get("/", auth, async (req, res) => {
  try {
    let sql = 'SELECT products.id,products.name AS product_name,products.status,status.name AS status_name,products.date_create,products.date_modify FROM products LEFT JOIN status ON products.status = status.id ORDER BY products.id DESC;';

    let products = await queryData(sql);

    if (typeof products !== 'undefined' && products.length > 0) {

      let arrOfData = [];

      for (let i = 0; i < products.length; i++) {
        let data = {
          "product_id": products[i].id,
          "name": products[i].product_name,
          "status": products[i].status_name,
          "date_create": getDateFormatTH(products[i].date_create),
          "date_midify": getDateFormatTH(products[i].date_modify)
        }
        arrOfData.push(data);
      }

      res.json(
        {
          data: arrOfData
        }
      )
    } else {
      res.json(
        {
          data: "We dont have products"
        }
      )
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/status/:status_id", auth, async (req, res) => {
  try {

    let { status_id } = req.params

    // Validate input
    if (!(status_id)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (isNaN(parseInt(status_id))) {
      res.status(400).send({ "data": "All input is required" });
    } else {
      let sql = `SELECT products.id,products.name,products.status,status.name,products.date_create,products.date_modify FROM products LEFT JOIN status ON products.status = status.id WHERE products.status=${status_id} ORDER BY products.id DESC;`;

      let products = await queryData(sql);

      if (typeof products !== 'undefined' && products.length > 0) {

        let arrOfData = [];

        for (let i = 0; i < products.length; i++) {
          let data = {
            "product_id": products[i].id,
            "name": products[i].name,
            "status": 'active',
            "date_create": getDateFormatTH(products[i].date_create),
            "date_midify": getDateFormatTH(products[i].date_modify)
          }
          arrOfData.push(data);
        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: "We dont have products"
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/:id", auth, async (req, res) => {
  try {

    let { id } = req.params;

    // Validate input
    if (!(id)) {
      res.status(400).send("All input is required");
    } else if (isNaN(parseInt(id))) {
      res.status(400).send("All input is required");
    } else {
      let sql = `SELECT products.id,products.name AS product_name,status.name AS status_name,products.date_create,products.date_modify FROM products LEFT JOIN status ON products.status = status.id WHERE products.id=${id} ORDER BY products.id DESC;`;
      let product = await queryData(sql);

      if (typeof product !== 'undefined' && product.length > 0) {

        let arrOfData = [];

        for (let i = 0; i < product.length; i++) {
          let data = {
            "product_id": product[i].id,
            "product_name": product[i].product_name,
            "status": product[i].status_name,
            "date_create": getDateFormatTH(product[i].date_create),
            "date_midify": getDateFormatTH(product[i].date_modify)
          }
          arrOfData.push(data);

        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: "We dont have product"
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.put("/", auth, async (req, res) => {
  try {
    let { productID, name, statusID } = req.body;

    // Validate input
    if (!(productID && name && statusID)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (productID !== null && name !== null && statusID !== null ) {
      let queryStatus = `SELECT id FROM status WHERE id =${statusID};`;
      let statusOfID = await queryData(queryStatus);

      if (typeof statusOfID !== 'undefined' && statusOfID.length > 0) {
        let queryProductByID = `SELECT * FROM products WHERE id='${productID}';`;
        let product = await queryData(queryProductByID);

        if (typeof product !== 'undefined' && product.length > 0) {
          let queryUpdateNameOfProduct = `UPDATE products SET name = '${name}', status = '${statusID}' WHERE id = '${productID}';`;
          await queryData(queryUpdateNameOfProduct);
          res.json(
            {
              'data': 'update product success'
            }
          );
        } else {
          res.json(
            {
              'data': 'We dont have product'
            }
          );
        }
      } else {
        res.json(
          {
            'data': 'We dont have status'
          }
        );
      }
    } else {
      res.status(400).send({ "data": "All input is required" });
    }
  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
});

router.post("/", auth, async (req, res) => {
  try {
    let { name } = req.body;
    // Validate input
    if (!(name)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (typeof name === 'string' || typeof name === 'number') {
      let queryInsertNewProduct = `INSERT INTO products(name) VALUES('${name}')`
      await queryData(queryInsertNewProduct);

      res.status(201).json(
        {
          'data': 'create product success'
        }
      );
    } else {
      res.status(400).send({ "data": "All input is required" });

    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
});

module.exports = router;
