var express = require("express");
var router = express.Router();

const auth = require("../middleware/auth");

const { queryData, getDateFormatTH } = require("../model/db")

router.get("/all", auth, async (req, res) => {
  try {

    let queryStatus = `SELECT * FROM status;`;
    let status = await queryData(queryStatus);

    if (typeof status !== 'undefined' && status.length > 0) {
      let arrOfData = [];

      for (let i = 0; i < status.length; i++) {
        let data = {
          "stat_idus": status[i].id,
          "name": status[i].name,
          "date_create": getDateFormatTH(status[i].date_create)
        }
        arrOfData.push(data);
      }

      res.json(
        {
          data: arrOfData
        }
      )
    } else {
      res.json(
        {
          data: "We dont have status"
        }
      )
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

module.exports = router;
