var express = require("express");
var router = express.Router();

const bcrypt = require("bcryptjs")
const jwt = require("jsonWebtoken")
const nodemailer = require('nodemailer')

const auth = require("../middleware/auth");

const { queryData, getDateFormatTH } = require("../model/db")

router.post("/", async (req, res) => {
  try {
    let { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (typeof email !== 'string' && typeof password !== 'number' || typeof password !== 'string') {
      res.status(400).send({ "data": "All input is required" });
    } else {

      const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      if (emailRegexp.test(email)) {
        let queryEmailOfUser = `SELECT email FROM users WHERE email='${email}';`;
        let oldEmail = await queryData(queryEmailOfUser);

        if (typeof oldEmail !== 'undefined' && oldEmail.length > 0) {
          return res.status(409).send({ "data": "User Already Exist. Please Login" });
        }

        //Encrypt user password
        encryptedPassword = await bcrypt.hash(password, 10);

        let queryInsertNewUser = `INSERT INTO users(email,password) VALUES ('${email}', '${encryptedPassword}')`;
        await queryData(queryInsertNewUser);

        // Create token
        let token = jwt.sign({
          data: 'foobar'
        }, 'shhhhh', { expiresIn: '24h' });

        res.status(201).json(
          {
            'email': email,
            'token': token
          }
        );
      } else {
        res.status(400).send({ "data": "All input is required" });
      }

    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
});

router.post("/login", async (req, res) => {
  try {
    let { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (typeof email !== 'string' && typeof password !== 'number' || typeof password !== 'string') {
      res.status(400).send({ "data": "All input is required" });
    } else {

      const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      if (emailRegexp.test(email)) {
        let queryPassWordOfEmail = `SELECT users.password,status.name AS status_name FROM users LEFT JOIN status ON status.id = users.status WHERE email='${email}';`;
        let passWordOfUser = await queryData(queryPassWordOfEmail);

        if (typeof passWordOfUser !== 'undefined' && passWordOfUser.length > 0 && passWordOfUser && (await bcrypt.compare(password, passWordOfUser[0].password))) {

          if (passWordOfUser[0].status_name === 'inactive') {
            res.json(
              {
                'data': 'User inactive please contact admin'
              }
            );
          } else {
            let token = jwt.sign({
              data: 'foobar'
            }, 'shhhhh', { expiresIn: '24h' });

            res.status(200).json(
              {
                'email': email,
                'token': token
              }
            );
          }


        } else {
          res.status(400).send({ "data": "Invalid Credentials" });
        }
      } else {
        res.status(400).send({ "data": "All input is required" });
      }


    }

  } catch (err) {
    res.send({ status: 500, msg: err.message });
  }
});

router.get("/all", auth, async (req, res) => {
  try {
    let queryUsers = `SELECT users.id,users.email,users.password,status.name,users.date_create,users.date_modify FROM users LEFT JOIN status ON status.id = users.status ORDER BY id DESC;`
    let users = await queryData(queryUsers);

    if (typeof users !== 'undefined' && users.length > 0) {

      let arrOfData = [];

      for (let i = 0; i < users.length; i++) {
        let data = {
          "user_id": users[i].id,
          "email": users[i].email,
          "status": users[i].name,
          "date_create": getDateFormatTH(users[i].date_create),
          "date_modify": getDateFormatTH(users[i].date_modify)
        }
        arrOfData.push(data);
      }

      res.json(
        {
          data: arrOfData
        }
      )
    } else {
      res.json(
        {
          data: "We dont have user"
        }
      )
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/status/:status_id", auth, async (req, res) => {
  try {

    let { status_id } = req.params

    // Validate input
    if (!(status_id)) {
      res.status(400).send("All input is required");
    } else if (isNaN(parseInt(status_id))) {
      res.status(400).send("All input is required");
    } else {
      let queryUser = `SELECT users.id,users.email,users.password,status.name,users.date_create,users.date_modify FROM users LEFT JOIN status ON status.id = users.status WHERE users.status = ${status_id} ORDER BY id DESC;`
      let users = await queryData(queryUser);

      if (typeof users !== 'undefined' && users.length > 0) {

        let arrOfData = [];

        for (let i = 0; i < users.length; i++) {

          let data = {
            "user_id": users[i].id,
            "email": users[i].email,
            "status": users[i].name,
            "date_create": getDateFormatTH(users[i].date_create),
            "date_modify": getDateFormatTH(users[i].date_modify)
          }
          arrOfData.push(data);

        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: "We dont have user"
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/:id", auth, async (req, res) => {
  try {
    let { id } = req.params;

    // Validate input
    if (!(id)) {
      res.status(400).send("All input is required");
    } else if (isNaN(parseInt(id))) {
      res.status(400).send("All input is required");
    } else {
      let queryUser = `SELECT users.id,users.email,users.password,status.name,users.date_create,users.date_modify FROM users LEFT JOIN status ON status.id = users.status WHERE users.id = ${id};`
      let user = await queryData(queryUser);

      let arrOfData = [];

      if (typeof user !== 'undefined' && user.length > 0) {
        for (let i = 0; i < user.length; i++) {
          let data = {
            "user_id": user[i].id,
            "email": user[i].email,
            "status": user[i].name,
            "date_create": getDateFormatTH(user[i].date_create),
            "date_modify": getDateFormatTH(user[i].date_modify)
          }
          arrOfData.push(data);
        }
        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.status(400).send({ "data": "Invalid Credentials" });
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.get("/:user_id/history", auth, async (req, res) => {
  try {

    let { user_id } = req.params;

    // Validate user input
    if (!(user_id)) {
      res.status(400).send({ "data": "All input is required" });
    } else if (isNaN(parseInt(user_id))) {
      res.status(400).send({ "data": "All input is required" });
    } else {

      let queryOrderOfUser = `SELECT users.id as user_id,users.email,orders.id AS order_id, status.name AS status_name FROM orders_products LEFT JOIN orders ON orders.id = orders_products.order_id LEFT JOIN users ON users.id = orders.user_id LEFT JOIN status ON status.id = orders.status WHERE users.id = ${user_id} GROUP BY orders.id`;
      let orderHistoryOfUser = await queryData(queryOrderOfUser);

      if (typeof orderHistoryOfUser !== 'undefined' && orderHistoryOfUser.length > 0) {
        let idOfUser = orderHistoryOfUser[0].id;
        let emailOfUser = orderHistoryOfUser[0].email;

        let arrOfData = {
          "user_id": idOfUser,
          "email": emailOfUser,
          "orders": {
            "active": [],
            "cancle": []
          }
        };

        for (let i = 0; i < orderHistoryOfUser.length; i++) {
          console.log(orderHistoryOfUser[i].status_name);
          if (orderHistoryOfUser[i].status_name === "active") {
            let data = {
              order_id: orderHistoryOfUser[i].order_id
            }
            arrOfData.orders.active.push(data);
          } else {
            let data = {
              order_id: orderHistoryOfUser[i].order_id
            }
            arrOfData.orders.cancle.push(data);
          }
        }

        res.json(
          {
            data: arrOfData
          }
        )
      } else {
        res.json(
          {
            data: 'user dont have order'
          }
        )
      }
    }

  } catch (error) {
    res.send({ status: 500, "data": error.message });
  }
})

router.put("/email", auth, async (req, res) => {
  try {
    let { userID, email, password } = req.body;

    // Validate input
    if (!(email && password && userID)) {
      res.status(400).send("All input is required");
    } else if (isNaN(email) && isNaN(password) && isNaN(userID)) {
      res.status(400).send("All input is required");
    } else {
      let queryEmailOfUser = `SELECT email FROM users WHERE email='${email}';`;
      let oldEmail = await queryData(queryEmailOfUser);

      if (typeof oldEmail !== 'undefined' && oldEmail.length > 0) {
        return res.status(400).send({ data: "Email dublicate" });
      }

      let queryPassWordOfUser = `SELECT password FROM users WHERE  id =${userID};`;
      let passWordOfUser = await queryData(queryPassWordOfUser);

      if (typeof passWordOfUser !== 'undefined' && passWordOfUser.length > 0) {

        if (passWordOfUser && (await bcrypt.compare(password, passWordOfUser[0].password))) {

          let queryUpdateEmailOfUser = `UPDATE users SET email = '${email}' WHERE id =${userID};`
          await queryData(queryUpdateEmailOfUser);

          res.json(
            {
              'data': 'update email success'
            }
          );
        } else {
          res.status(400).send("Invalid Credentials");
        }
      } else {
        res.status(400).send("Invalid Credentials");
      }
    }


  } catch (err) {
    res.send({ status: 500, msg: err.message });
  }
});

router.put("/status", auth, async (req, res) => {
  try {
    let { userID, statusID } = req.body;

    // Validate input
    if (!(userID && statusID)) {
      res.status(400).send("All input is required");
    } else if (isNaN(userID) && isNaN(statusID)) {
      res.status(400).send("All input is required");
    } else {
      let queryStatus = `SELECT id FROM status WHERE id =${statusID};`;
      let status = await queryData(queryStatus);

      if (typeof status !== 'undefined' && status.length > 0) {

        let queryUser = `SELECT id FROM users WHERE  id =${userID};`;
        let user = await queryData(queryUser);

        if (typeof user !== 'undefined' && user.length > 0) {

          let queryUpdateStatusOfUser = `UPDATE users SET status = '${statusID}' WHERE id =${userID};`
          await queryData(queryUpdateStatusOfUser);

          res.json(
            {
              'data': 'update status user success'
            }
          );

        } else {
          res.status(400).send("Invalid Credentials");
        }
      } else {
        res.status(400).send({ "data": "We dont have status" });
      }
    }

  } catch (err) {
    res.send({ status: 500, msg: err.message });
  }
});

router.put("/change_password", auth, async (req, res) => {
  try {
    let { email, password } = req.body;

    // Validate input
    if (!(email && password)) {
      res.status(400).send("All input is required");
    } else if (isNaN(email) && isNaN(password)) {
      res.status(400).send("All input is required");
    } else {
      let queryEmailOfUser = `SELECT email,id FROM users WHERE email='${email}';`;
      let user = await queryData(queryEmailOfUser);

      if (typeof user !== 'undefined' && user.length > 0) {

        //Encrypt user password
        encryptedPassword = await bcrypt.hash(password, 10);

        let queryUpdateEmailOfUser = `UPDATE users SET password = '${encryptedPassword}' WHERE id =${user[0].id};`
        await queryData(queryUpdateEmailOfUser);

        // let setttingEmailAdmin = nodemailer.createTransport({
        //   service: 'gmail',
        //   auth: {
        //     user: 'emailAdmin@gmail.com',
        //     pass: 'passwordAdminOfemailAdmin'
        //   }
        // });

        // let textShowInEmail = `
        // <p>เรียน email ${emailOfUser[0].email}</p>
        // <p>&nbsp;&nbsp;&nbsp;&nbsp; Username: ${emailOfUser[0].email}<br/>
        // &nbsp;&nbsp;&nbsp;&nbsp; Password: ${password}</p><br/>
        // คุณสามารถเข้าสู่ระบบ My Project ด้วย Username และ Password นี้
        // ขอบคุณที่ใช้บริการระบบ My Project
        // <br/><br/>
        // Admin<br/>
        // My Project<br/>
        // `
        // const mailOption = {
        //   from: 'emailAdmin@gmail.com',
        //   to: `${emailOfUser[0].email}`,
        //   subject: '[My Project] แจ้งเตือนการขอรีเซ็ตรหัสผ่าน ผู้ใช้งาน',
        //   html: textShowInEmail
        // }

        // // Send mail
        // setttingEmailAdmin.sendMail(mailOption, function (err, data) {
        //   if (err) {
        //     res.status(500).send({ status: 'Bad Request', result: err })
        //   } else {
        res.send({ data: 'Reset password Success' })
        //   }
        // })
      } else {
        res.status(400).send("Invalid Credentials");
      }
    }

  } catch (error) {
    res.status(500).send({ result: err })

  }


})

module.exports = router;
